# import serial
import time
import datetime
import json
import MySQLdb

ser = serial.Serial('ttyUSB0', 4800, timeout=3)
print ser.isOpen()
print ser.name
print ser.inWaiting()

while 1==1 :
    while ser.inWaiting() == 0 :
        time.sleep(0.5)
    print ser.readline()


jsonDecoded = json.loads('{"sensors":[{"serial":10,"values":[]},{"serial":2,"values":[]},{"serial":1,"values":[0.00000,1.00000,2.00000,3.00000,4.00000,5.00000,6.00000,7.00000,8.00000,9.00000,10.00000,11.00000,12.00000,13.00000,14.00000,15.00000,16.00000,17.00000,18.00000,19.00000,20.00000,21.00000,22.00000,23.00000]}],"arduino":{"serial":1}}'
)

db = MySQLdb.connect(host="localhost", # your host, usually localhost
                     user="root", # your username
                      passwd="", # your password
                      db="hive") # name of the data base

cur = db.cursor()

# get arduino serial
arduinoSerial = jsonDecoded["arduino"]["serial"]

# for each sensor
for sensor in jsonDecoded["sensors"] :
    # get sensor serial
    sensorSerial = sensor["serial"]
    querySensorId = "SELECT sensors.id from sensors LEFT JOIN arduinos ON sensors.arduino_id = arduinos.id WHERE sensors.serial = " + str(sensorSerial) + " and arduinos.id = " + str(arduinoSerial) + " LIMIT 1"
    cur.execute(querySensorId)
    # get sensor id
    sensorId = cur.fetchone()
    if sensorId is not None:
      sensorId = sensorId[0]
      now = time.time();
      len = len(sensor["values"])
      # for each value (i => index, e => value)
      for i, e in list(enumerate(sensor["values"])):
        index = len-i-1
        value = e
        TIME_BETWEEN_ACQUISITION = 3600 # seconds
        time = datetime.datetime.utcfromtimestamp(now - 3600 * index).strftime('%Y-%m-%d %H:%M:%S')
        queryDb = "INSERT INTO data(value,sensor_id,created_at,updated_at) VALUES("+str(value)+","+str(sensorId)+",'"+str(time)+"','"+str(time)+"')"
        cur.execute(queryDb)
db.commit()


# {"sensors":[{"serial":1,"values":[]},{"serial":2,"values":[]},{"serial":3,"values":[0.00000,1.00000,2.00000,3.00000,4.00000,5.00000,6.00000,7.00000,8.00000,9.00000,10.00000,11.00000,12.00000,13.00000,14.00000,15.00000,16.00000,17.00000,18.00000,19.00000,20.00000,21.00000,22.00000,23.00000]}],"arduino":{"serial":1}}