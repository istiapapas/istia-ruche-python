var com = require("serialport");

// MYSQL
var mysql      = require('mysql');
var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'ruches_development'
});

//SERIAL
//var serialPort = new com.SerialPort("COM4", {
var serialPort = new com.SerialPort("/dev/ttyAMA0", {
    baudrate: 9600,
    parser: com.parsers.readline('\r\n')
  });
serialPort.on('open',function() {
  	console.log('Port open');
});

serialPort.on('data', function(data) {
  	console.log(data);
  	result = JSON.parse(data);
  	for (var i = result.sensors.length - 1; i >= 0; i--) {
      var date = new Date().toISOString();
      var row = {};
      row.sensor_id = result.sensors[i].id;
      row.value = result.sensors[i].value;
      row.created_at = date;
  		row.updated_at = date;
		  var query = connection.query('INSERT INTO data SET ?', row, function(err, result) {});
  	};
});

serialPort.on('close',function() {
	console.log('Port closed');
});